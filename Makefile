include .env

dc-up:
	@docker-compose up -d

dc-down:
	@docker-compose down

dc-build:
	@if [ -d "bootstrap/cache" ]; then sudo chown -R ${USER}:${USER} bootstrap/cache; fi
	@sudo chown -R ${USER}:${USER} storage
	@sudo chown -R ${USER}:${USER} app
	@sudo chown -R ${USER}:${USER} resources
	@sudo chmod -R 755 bootstrap
	@sudo chmod -R 777 storage
	@docker-compose up --build -d
	@if ! [ -f ".env" ]; then cp .env.example .env; php artisan key:generate; fi

perm:
	@if [ -d "bootstrap/cache" ]; then sudo chown -R ${USER}:${USER} bootstrap/cache; fi
	@sudo chown -R ${USER}:${USER} storage
	@sudo chown -R ${USER}:${USER} app
	@sudo chown -R ${USER}:${USER} resources
	@sudo chmod -R 755 bootstrap
	@sudo chmod -R 777 storage

art:
	@docker exec pesky-ewok_php-fpm_1 php artisan ${ARGS}

run:
	@docker exec pesky-ewok_php-fpm_1 ${ARGS}

db-create:
	@docker exec laravel_blog_postgres_1 psql -c "CREATE DATABASE ${DB_DATABASE};"
	@docker exec laravel_blog_postgres_1 psql -c "CREATE USER ${DB_USERNAME} WITH ENCRYPTED PASSWORD '${DB_PASSWORD}';"
	@docker exec laravel_blog_postgres_1 psql -c "GRANT ALL PRIVILEGES ON DATABASE ${DB_DATABASE} TO ${DB_USERNAME};"
