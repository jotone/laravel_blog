<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'name',
        'slug',
        'content',
        'author_id',
        'img_url'
    ];


    public function author()
    {
        return $this->belongsTo(User::class, 'id', 'author_id');
    }

    public function views()
    {
        return $this->hasMany(Views::class, 'post_id', 'id');
    }
}
