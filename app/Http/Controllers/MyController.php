<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;

class MyController
{
    public function method( $id = null, Request $request ) {
        return view('my_template', [
            'title' => 'Hello',
            'id' => '<b>'.$id.'</b>'
        ]);
    }
}