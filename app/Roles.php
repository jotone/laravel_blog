<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = ['name', 'slug', 'route', 'crud'];

    public function canRead()
    {
        return $this->checkCrud('r');
    }

    public function canCreate()
    {
        return $this->checkCrud('c');
    }

    public function canUpdate()
    {
        return $this->checkCrud('u');
    }

    public function canDelete()
    {
        return $this->checkCrud('d');
    }

    private function checkCrud($char)
    {
        return strpos($this->crud, $char) >= 0;
    }

    public function roles()
    {
        return $this->belongsToMany(User::class, 'user_roles_pivot', 'role_id', 'user_id');
    }
}
