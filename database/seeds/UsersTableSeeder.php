<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 0; $i < 10; $i++) {
            $user = User::create([
                'name' => $faker->name,
                'password' => bcrypt('123456'),
                'email' => $faker->email,
                'about' => [
                    'img_url' => '',
                    'company' => $faker->jobTitle . ' в ' . $faker->company
                ]
            ]);
        }
    }
}
