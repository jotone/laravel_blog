<?php

use Illuminate\Database\Seeder;
use App\Posts;
use App\User;
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 100; $i++) {
            $name = $faker->sentence(5);
            $post = Posts::create([
                'name' => $name,
                'slug' => preg_replace('/\s+/','-', $name),
                'content' => $faker->paragraph(4),
                'author_id' => User::inRandomOrder()->value('id'),
                'img_url' => '',
            ]);
        }
    }
}
